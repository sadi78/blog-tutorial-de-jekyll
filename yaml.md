---
layout: page
title: YAML
---

_Lo mínimo indispensable_ que hay que saber de YAML para defenderse en Jekyll.

_Cheatsheet_: <http://lzone.de/cheat-sheet/YAML>

Como ejemplo, utilizaré el archivo `_config.yml` de este sitio, modificado ligeramente con fines educativos:

```yaml
title: Blog-tutorial de Jekyll
email: jlsalinas@example.com
description: >- # con esto ignoramos los retornos de carro hasta "twitter_username" 
  Tutorial
  de
  Jekyll
  en
  español
twitter_username: jotaelesalinas
github_username:  jotaelesalinas

# Build settings
theme: minima
plugins:
  - jekyll-feed
  - another-plugin

show_excerpts: true

y_ahora:
  unas_listas:
    anidadas: 123
    clave_2: otro valor
  y_aun_mas:
    anidacion:
      - texto 1
      - texto 2
```

- YAML está compuesto de variables, expresadas por un par _nombre-de-variable: valor_.

- El nombre de la variable es lo que hay antes de los dos puntos, por ejemplo `title`.

- El valor es lo que viene después de los dos puntos, que puede ser:

  - Un valor simple, como un número, una fecha, una cadena de texto que quepa en una línea o los valores `true` y `false`. Ejemplos: `email` y `show_excerpts`.

  - Una cadena de texto multilínea. En ese caso habrá que poner `>-` después de la clave y el texto
    en una o varias líneas nuevas sangradas otro nivel. Ejemplo: `description`.

  - Un listado. En tal caso, se pondrá cada una de ellas en una línea nueva, sangradas todas otro nivel, comenzando por `-` y espacio. Ejemplo: `plugins`, que tiene dos elementos.

- El sangrado importa. Cuando hay sub-elementos, los espacios al principio de la línea determinan en qué nivel se encuentra el elemento. Se recomienda utilizar una sangría de 2 espacios.

- El carácter `#` indica que lo que viene detrás es un comentario y debe ser ignorado.

- Se pueden anidar elementos. Baste con saber esto.
