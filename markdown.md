---
layout: page
title: Markdown
---

_Lo mínimo indispensable_ que hay que saber de Markdown para defenderse en Jekyll.

_Cheatsheet_: <https://aksakalli.github.io/jekyll-doc-theme/docs/cheatsheet/>

## Párrafos, formato, enlaces

```markdown
Un párrafo está separado de los elementos anteriores y posteriores
por una línea en blanco.

Si no, se considera que
está
todo en el mismo
párrafo.

Un poco de texto en **negrita**, _itálica_, **_ambas_** y ~~tachado~~.

URL sin texto: <http://example.com>

Un [enlace con texto descriptivo](http://example.com "Texto al posar el ratón").

[Enlace referenciado con un número][1]

[Enlace referenciado con texto][informe anual]

[1]: http://example.com/ref1.html
[informe anual]: http://example.com/informe-anual.pdf
```

Un párrafo está separado de los elementos anteriores y posteriores
por una línea en blanco.

Si no, se considera que
está
todo en el mismo
párrafo.

Un poco de texto en **negrita**, _itálica_, **_ambas_** y ~~tachado~~.

URL sin texto: <http://example.com>

Un [enlace con texto descriptivo](http://example.com "Texto al posar el ratón").

[Enlace referenciado con un número][1]

[Enlace referenciado con texto][informe anual]

[1]: http://example.com/ref1.html
[informe anual]: http://example.com/informe-anual.pdf

---

<br>

## Encabezados

Se ponen al principio de la línea tantos `#` como el nivel del encabezado.

```markdown
# Titulo

## Sección 1

### Sección 1.1

### Sección 1.2 

#### Sección 1.2.1 
```

# Titulo

## Sección 1

### Sección 1.1

### Sección 1.2 

#### Sección 1.2.1 

---

<br>

## Imágenes

```markdown
![Esta imagen sí existe](https://github.com/adam-p/markdown-here/raw/master/src/common/images/icon48.png "Texto mostrado al pasar el ratón")

![Esta imagen no existe](https://example.com/imagen.png "Texto mostrado al pasar el ratón")

![Esta imagen sí existe][logo 1]

![Esta imagen no existe][logo 2]

[logo 1]: https://github.com/adam-p/markdown-here/raw/master/src/common/images/icon48.png "Logo de markdown-here"
[logo 2]: http://example.com/logo.png "Logo de example.com"
```

![Esta imagen sí existe](https://github.com/adam-p/markdown-here/raw/master/src/common/images/icon48.png "Texto mostrado al pasar el ratón")

![Esta imagen no existe](https://example.com/imagen.png "Texto mostrado al pasar el ratón")

![Esta imagen sí existe][logo 1]

![Esta imagen no existe][logo 2]

[logo 1]: https://github.com/adam-p/markdown-here/raw/master/src/common/images/icon48.png "Logo de markdown-here"
[logo 2]: http://example.com/logo.png "Logo de example.com"

---

<br>

## Listas

### Listas desordenadas (_bullet points_)

```markdown
- Elemento 1
    - Subelemento 1
        - Otro subelemento
    - Subelemento 2
- Elemento 2

  Se pueden meter párrafos si se sangran correctamente,
  pero tienen que estar separados por una línea en blanco.
- Elemento 3
  Si no hay una línea en blanco se entiende que es el mismo párrafo.
```

- Elemento 1
    - Subelemento 1
        - Otro subelemento
    - Subelemento 2
- Elemento 2

  Se pueden meter párrafos si se sangran correctamente,
  pero tienen que estar separados por una línea en blanco.
- Elemento 3
  Si no hay una línea en blanco se entiende que es el mismo párrafo.

### Listas ordenadas

```markdown
1. Primer elemento
1. Los numeros no importan: esto saldrá con un 2
    1. Un elemento anidado, dejando 4 espacios
    1. Otro elemento anidado, el número es automático
1. Tercer elemento
    a. No se puede anidar con letras...
1. Cuarto elemento
    4.1. ...Ni con número-punto-número

Y ahora...

5. No podemos continuar por el 5
```

1. Primer elemento
1. Los numeros no importan: esto saldrá con un 2
    1. Un elemento anidado, dejando 4 espacios
    1. Otro elemento anidado, el número es automático
1. Tercer elemento
    a. No se puede anidar con letras...
1. Cuarto elemento
    4.1. ...Ni con número-punto-número

Y ahora...

5. No podemos continuar por el 5

---

<br>

## Tablas

```markdown
| Nombre | Apellido | Ciudad | Edad |
|-|-|-|-:|
| Héctor | Coco | Veracruz | 105 |
| Agustín | Conesa | Botella | 8 |
```

| Nombre | Apellido | Ciudad | Edad |
|-|-|-|-:|
| Héctor | Coco | Veracruz | 105 |
| Agustín | Conesa | Botella | 8 |

---

<br>

## HTML "a pelo"

Se puede incluir HTML en un documento Markdown. Pero no se podrá utilizar markdown dentro del HTML
(prestad atención a las marcas de formato en Markdown y HTML).

```html
<h4>Glosario</h4>

<dl>
  <dt>Vocablo #1</dt>
    <dd>Definición con **negrita** e _itálica_.</dd>
  <dt>Palabro #2</dt>
    <dd>Definición con <strong>negrita</strong> e <em>itálica</em>.</dd>
</dl>
```

<h4>Glosario</h4>

<style>
  dt {
    margin-left: 1em;
    font-style: italic;
  }
  dd {
    margin-left: 2em;
  }
</style>

<dl>
  <dt>Vocablo #1</dt>
    <dd>Definición con **negrita** e _itálica_.</dd>
  <dt>Palabro #2</dt>
    <dd>Definición con <strong>negrita</strong> e <em>itálica</em>.</dd>
</dl>

---

<br>

## Clases CSS

Se puede añadir una clase CSS a una imagen o a un párrafo:

```markdown
<style>
  .logo { border: 2px solid red; }
  .importante { border: 1px solid darkred; color: darkred; padding: 1.5em; }
</style>

![Logo](https://github.com/adam-p/markdown-here/raw/master/src/common/images/icon48.png){:.logo}

**Nota**: Esto es un párrafo súper importante.
{:.importante}
```

<style>
  .logo { border: 2px solid red; }
  .importante { border: 1px solid dimgrey; background-color: lightblue; padding: 1.5em; }
</style>

![Logo](https://github.com/adam-p/markdown-here/raw/master/src/common/images/icon48.png){:.logo}

**Nota**: Esto es un párrafo súper importante.
{:.importante}

---

<br>

## Texto plano y código

Hay varias formas de mostrar _texto plano_ con Markdown.

Sangrando cuatro espacios:

```
    En este texto no se interpretarán
    las <strong>etiquetas</strong> HTML
    ni las **marcas** de Markdown.
```

Encerrando el texto entre tres _backticks_ (`` ` ``):

    ```
    En este texto no se interpretarán
    las <strong>etiquetas</strong> HTML
    ni las **marcas** de Markdown.
    ```

La ventaja de este método, sobre todo para programadores, es que se puede especificar un lenguaje y aparecerá coloreados de acuerdo a la sintaxis del mismo, si se encuentra entre los reconocidos.

    ```html
    <h1 class="title">Título</h1>
    ```

```html
<h1 class="title">Título</h1>
```

    ```javascript
    if (typeof url == 'undefined') url = document.location.href;
    ```

```javascript
if (typeof url == 'undefined') url = document.location.href;
```

    ```python
    squares = [str.lower() for str in words if str != 'Hello']
    ```

```python
squares = [str.lower() for str in words if str != 'Hello']
```

También se puede marcar una o varias palabras como código, o bien resaltarla para darle importancia, metiéndola entre backticks:

```
Para generar el sitio habrá que ejecutar `jekyll build`.
```

Para generar el sitio habrá que ejecutar `jekyll build`.

En sitios que no sean sobre programación/informática, se pueden utilizar los backticks para resaltar palabras o conceptos de otra forma distinta a la negrita:

```markdown
<style>
.resaltado code {
    background-image: linear-gradient(-100deg,hsla(60,92%,75%,.4),hsla(60,92%,75%,.7) 75%,hsla(60,92%,75%,.1));
    border-radius: .25em 0;
    padding: 0 .25em;
    margin: 0 -.25em;
    border: inherit;
    background-color: inherit;
    border-radius: inherit;
    padding: 0 2px;
}
</style>
Una frase con una palabra `resaltada`.
{:.resaltado}
```

<style>
.resaltado > code {
    display: inline-box;
    background-image: linear-gradient(-100deg,hsla(60,100%,55%,.4),hsla(60,100%,55%,.7) 75%,hsla(60,100%,55%,.9));
    border-radius: .25em 0;
    padding: 0 .25em;
    margin: 0 -.25em;
    border: inherit;
    background-color: inherit;
    border-radius: inherit;
}
</style>
Una frase con una palabra `resaltada`.
{:.resaltado}

```markdown
<style>
.subrayado > code {
    background-image: linear-gradient(120deg, darkorange 0%, orange 100%);
    background-repeat: no-repeat;
    background-size: 100% 0.2em;
    background-position: 0 96%;
    border: inherit;
    background-color: inherit;
    border-radius: inherit;
    padding: 0 2px;
}
</style>
Una frase con una palabra `subrayada`.
{:.subrayado}
```

<style>
.subrayado > code {
    background-image: linear-gradient(120deg, darkorange 0%, orange 100%);
    background-repeat: no-repeat;
    background-size: 100% 0.2em;
    background-position: 0 96%;
    border: inherit;
    background-color: inherit;
    border-radius: inherit;
    padding: 0 2px;
}
</style>
Una frase con una palabra `subrayada`.
{:.subrayado}

Si se quisiera utilizar siempre uno de estos dos estilos por defecto, habría que modificar la hoja de estilos del sitio web.
