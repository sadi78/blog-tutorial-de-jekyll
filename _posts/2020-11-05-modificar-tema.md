---
layout: post
title:  Modificar archivos del tema
date:   2020-11-05 22:22:35 +0100
categories:
---
Si queremos modificar la apariencia del sitio web de una forma mas profunda que con los estilos CSS, será necesario tocar los archivos del tema.

Por ejemplo, es posible que queramos:

- traducir ciertas palabras o frases al español
- añadir cosas que no existen, por ejemplo enlaces a entradas relacionas del blog
- quitar cosas que no queramos, como los enlaces a redes sociales
- mover cosas de sitio, por ejemplo la fecha y categorías de una entrada, al final del contenido 

## Qué es un tema

Jekyll tiene varias carpetas predefinidas para plantillas y estilos:

- `_includes` contiene trozos de código que utilizamos a menudo en nuestro sitio, por ejemplo una ficha de jugador en un blog dedicado a la Liga de Fútbol. Un _include_ puede contener otros _includes_ dentro.
- `_layouts` contiene plantillas que definen la estructura de los diferentes tipos de páginas de nuestro sitio. Por ejemplo, la página principal suele tener un _layout_ específico, las páginas otro, las entradas del blog otro, los listados por categorías otro, y así. Normalmente hay un _layout_ base, que se suele llamar `default`, del que derivan los demás.
- `_sass` contiene los estilos CSS, pero en un lenguaje mas avanzado que permite introducir variables y otras cosas útiles.
- `assets` contiene archivos que serán copiados tal cual.

Si nos sentimos orgullosos de la estructura y apariencia de nuestro sitio web, Jekyll nos permite "empaquetar" estas carpetas dentro de un archivo que podemos compartir y publicar para que lo utilicen otros usuarios de Jekyll.

Este no es el caso para el común de los mortales. Afortunadamente para nosotros, hay gente muy buena diseñando sitios webs, y publican temas que podemos instalar fácilmente, unos gratis y otros pagando.

Hay muchas webs de temas para Jekyll, como por ejemplo <http://jekyllthemes.org/>.

Un tema gratuito que a mí me gusta mucho es [Zolan](http://jekyllthemes.org/themes/zolan/) de
[Artem Sheludko](https://artemsheludko.github.io).

**Importante**: Antes de utilizar un tema, hay que comprobar que la licencia de uso nos permite utilizarlo para nuestros propósitos, si estamos obligados a publicar el nombre y autor del tema, etc.

## Cómo funciona un tema

Cuando instalamos un tema, Jekyll lo descarga (si no lo tenemos ya) y lo descomprime en una carpeta externa, fuera de nuestro proyecto.

Si hacemos referencia a un archivo que se supone que debe estar en una de las carpetas antes mencionadas, por ejemplo si en una página especificamos `layout: contacto`, Jekyll lo buscará:

- Inicialmente, en nuestra carpeta de trabajo `./_layouts/contacto.html`.
- Si no lo encuentra, en la carpeta del tema que tengamos configurado `CARPETA_DEL_TEMA_ACTUAL/_layouts/contacto.html`.
- Si tampoco lo encuentra ahí, dará un error.

Para instalar un tema hay que seguir las instrucciones que nos den en la página oficial del tema.

Una vez instalado, hay que decirle a Jekyll en `_config.yml` qué tema queremos utilizar:

```yaml
theme: minima
```

## Trayéndonos los archivos del tema

Para saber dónde se encuentran los archivos del tema que Jekyll trae por defecto, [minima](https://github.com/jekyll/minima), ejecutaremos:

```
bundle info minima
```

En mi caso, esta es la información que obtuve:

```
  * minima (2.5.1)
        Summary: A beautiful, minimal theme for Jekyll.
        Homepage: https://github.com/jekyll/minima
        Path: /var/lib/gems/2.7.0/gems/minima-2.5.1
```

Este es el contenido de mi carpeta `/var/lib/gems/2.7.0/gems/minima-2.5.1`:

```
jl@DESKTOP:~/sitios/mi-super-web$ ll /var/lib/gems/2.7.0/gems/minima-2.5.1
total 16
drwxr-xr-x 1 root root  512 Oct 27 22:22 ./
drwxr-xr-x 1 root root  512 Oct 27 22:22 ../
-rw-r--r-- 1 root root 1115 Oct 27 22:22 LICENSE.txt
-rw-r--r-- 1 root root 8848 Oct 27 22:22 README.md
drwxr-xr-x 1 root root  512 Oct 27 22:22 _includes/
drwxr-xr-x 1 root root  512 Oct 27 22:22 _layouts/
drwxr-xr-x 1 root root  512 Oct 27 22:22 _sass/
drwxr-xr-x 1 root root  512 Oct 27 22:22 assets/
```

Por lo tanto, en mi caso yo tuve que ejecutar:

```
cp -r /var/lib/gems/2.7.0/gems/minima-2.5.1/_includes .
cp -r /var/lib/gems/2.7.0/gems/minima-2.5.1/_layouts .
cp -r /var/lib/gems/2.7.0/gems/minima-2.5.1/_sass .
cp -r /var/lib/gems/2.7.0/gems/minima-2.5.1/assets .
```

Lo mas probable es que en vuestro caso la carpeta sea diferente, con una versión mas moderna.

## Guardar cambios

Antes de toquetear nada en los archivos del tema, este es el momento erfecto para "commitear" los cambios con git o GitHub Desktop.

## Cambios básicos

En el archivo `_sass/minima.scss` tenéis muchas variables que podéis modificar a vuestro gusto. SCSS es un lenguaje muy similar a CSS.

Las modificaciones que hagáis en este archivo no alterarán la estructura de las páginas, solo el aspecto visual de los diferentes elementos.

Podéis jugar cambiando fuentes, tamaños, colores...

Como sé que os lo estáis preguntando, Comic Sans se pone así:

```scss
$base-font-family: "Comic Sans MS", ...resto de fuentes...;
```

Aunque al final del archivo hay otros archivos importados, yo no me aventuraría a modificarlos... pero sin riesgo no hay gloria.
