---
layout: post
title:  Subir el sitio a GitLab Pages
date:   2020-11-12 22:22:35 +0100
categories:
---
A continuación vamos a ver cómo subir nuestro sitio web a [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/) automáticamente cada vez que _pusheemos_ a `master`. No solo eso, sino que antes comprobaremos que el sitio se genera correctamente y que todos los enlaces funcionan.

Una particularidad de GitLab Pages es que **no podemos decidir la URL** de nuestro sitio, sino que será forzosamente **https://USUARIO.gitlab.io/REPOSITORIO**. En mi caso, <https://sadi78.gitlab.io/blog-tutorial-de-jekyll/>. Por ello tendremos que asegurarnos de que el sitio web funciona corectamente en este subdirectorio en vez del directorio raíz con el que trabajamos en local.

Para automatizar el proceso de publicar nuestro sitio utilizaremos [GitLab Pipelines](https://docs.gitlab.com/ee/ci/pipelines/), que nos permite ejecutar tareas en servidores alojados por ellos mismos. Es un servicio gratuito, hasta un límite de minutos de ejecución al día. GitHub tiene lo mismo, y se llama [GitHub Actions](https://github.com/features/actions).

Pipelines ejecutará las instrucciones que nosotros le digamos, una después de otra, bien hasta que alguna de ellas falle, deteniéndose y finalizando con error, o bien hasta que el pipeline finalice con éxito. Para organizarnos mejor, podemos agrupar las órdenes en trabajos, _jobs_ o _stages_.

Si al terminar nuestro pipeline hemos dejado disponible una carpeta que se llame `public` (ya veremos cómo se hace), GitLab la publicará automáticamente.

**Importante**: Para activar Pipelines necesitaremos un archivo `.gitlab-ci.yml` en la carpeta raiz de nuestro proyecto.

Podéis ver el contenido del archivo `.gitlab-ci.yml` de este sitio aquí: <https://gitlab.com/sadi78/blog-tutorial-de-jekyll/-/blob/master/.gitlab-ci.yml>.

## .gitlab-ci.yml paso a paso:

```yaml
image: ruby:latest
```

Para ejecutar nuestras órdenes necesitaremos un entorno, un sistema operativo con su software. Para ello, GitLab Pipelines utiliza [Docker](https://www.docker.com/). Con este software podemos "imitar" casi cualquier sistema operativo dentro de otro, y con el software que nos interese ya preinstalado.

En este caso, lo primero que estamos diciendo es que queremos ejecutar nuestras órdenes en un sistema operativo Linux con la última versión de Ruby instalada (recordad que Jekyll se ejecuta sobre Ruby).

Siempre hay que intentar cargar la imagen que tenga el software mínimo indispensable para nuestras necesidades, ya que instalar dependencias lleva tiempo y se paga por tiempo de ejecución.

```yaml
stages:
  - build
  - test
  - deploy
```

Aquí estamos diciendo que vamos a ejecutar tres etapas o trabajos (grupos de instrucciones). Primero `build`, si finaliza correctamente, `test` y si también finaliza correctamente, `public`.

Aunque los detalles de los trabajos se encuentren después en otro orden, se ejecutarán en el orden establecido aquí.

```yaml
variables:
  JEKYLL_ENV: production
  LC_ALL: C.UTF-8
  DOCKER_DRIVER: overlay2

cache:
  paths:
    - vendor/
```

Un poco de configuración general y para acelerar el proceso de generación del sitio.

```yaml
generar:
  stage: build
  script:
    - gem install bundler
    - bundle install --path vendor
    - bundle exec jekyll build -d ./blog-tutorial-de-jekyll --config "_config.yml,_config-gitlab-pages.yml"
  artifacts:
    expire_in: 1 hour
    paths:
      - blog-tutorial-de-jekyll
```

Entramos en faena con el primer trabajo, `build`, que generará nuestro sitio web.

- La línea `build:` la podríamos llamar de otra forma, p.e. `Generar:`, pero yo suelo usar el mismo nombre que el trabajo.

- `stage: build` es para indicar que este grupo es `build`.

- `script` es una lista de órdenes, que se ejecutarán una después de la otra mientras ninguna falle.

- `gem install bundler` instala `bundler`, necesario para instalar las dependencias de Jekyll.

- `bundle install --path vendor` instala las dependencias de Jekyll en la carpeta `vendor`.

- `bundle exec jekyll build ...` genera el sitio web. Los parámetros son:

    - Con `-d ./blog-tutorial-de-jekyll` le decimos que guarde el sitio web generado en la carpeta `blog-tutorial-de-jekyll` en vez de `_site`.
    - Con `--config` lee la configuración de dos archivos, primero del normal `_config.yml` y después de `_config-gitlab-pages.yml`, que sobreescribe una variable necesaria para que el sitio web se vea bien en GitLab Pages.

- `artifacts` le dice a GitLab que deje una carpeta disponible para el trabajo siguiente, en este caso `blog-tutorial-de-jekyll`.

Si cualquiera de estos pasos fallase por cualquier motivo, el trabajo y el pipeline se detendrían.

Al terminar un trabajo, se borra todo y el siguiente trabajo comienza desde cero con una imagen limpia. Solo lo que hayamos pasado con `artifacts` estará disponible en el siguiente trabajo.

```yaml
testear:
  stage: test
  script:
    - apt-get update -qy && apt-get install webfs httpie linkchecker -y
    - webfsd -p 8080 -r . -f index.html
    - http GET http://localhost:8080/blog-tutorial-de-jekyll/index.html -h | grep OK
    - http GET http://localhost:8080/blog-tutorial-de-jekyll/pruebas/html1.html -h | grep OK
    - linkchecker "--ignore-url=^mailto:" http://localhost:8080/blog-tutorial-de-jekyll/
  artifacts:
    expire_in: 1 hour
    paths:
      - blog-tutorial-de-jekyll
```

En el segundo trabajo, `test`, comprobamos que el sitio se sirve correctamente y que no hay enlaces rotos.

**Importante**. En este paso ya no tenemos nuestro repositorio. Solo tenemos el artefacto que hayamos pasado desde el trabajo anterior, en este caso la carpeta `blog-tutorial-de-jekyll` con el contenido de nuestra web.

`apt-get ...` instala software necesario para hacer ciertas pruebas:

`webfsd ...` ejecuta un servidor web muy simple en local.

`http GET ...` carga varias páginas de nuestro sitio que sabemos que deben existir y comprueba que existen de verdad.

`linkchecker ...` comprueba que todos los enlaces de nuestro sitio web apuntan a páginas que existen.

De nuevo, volvemos a pasar la carpeta `blog-tutorial-de-jekyll` al siguiente trabajo.

```yaml
pages:
  stage: deploy
  only:
    - master
  script:
    - mv blog-tutorial-de-jekyll public
  artifacts:
    expire_in: 1 hour
    paths:
      - public
```

`mv blog-tutorial-de-jekyll public` renombra la carpeta `blog-tutorial-de-jekyll` a `public`.

`only` especifica que este trabajo se ejecute solo en ciertas condiciones.

`- master` indica que el trabajo se ejecute solo si la rama en la que estamos trabajando es `master`.

Con `artifacts`, dejamos la carpeta `public` disponible para el siguiente paso.
Al no haber más pasos, GitLab publicará lo que sea que hayamos dejado en esa carpeta en https://USUARIO.gitlab.io/REPOSITORIO .

## Ver el estado de los pipelines y los trabajos

En vuestra carpeta de proyecto en GitLab, hay que ir al menú lateral izquierdo, dentro de "CI/CD":

- En "Pipelines" veremos el estado de cada pipeline ejecutado (uno por _push_).

- En "Jobs" veremos el estado de cada uno de los trabajos de cada pipeline. Pinchando el ID del trabajo se puede ver los comandos ejecutados y la salida generada por cada uno de ellos.
