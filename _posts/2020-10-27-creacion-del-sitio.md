---
layout: post
title:  Creación de nuestro sitio web
date:   2020-10-27 20:22:35 +0100
categories:
---

Una vez tenemos Jekyll instalado, lo utilizaremos para crear un nuevo sitio web:

```
sudo jekyll new mi-super-web
cd mi-super-web
```

Ya está. Jekyll ha generado un sitio web para nosotros.

Antes de ver su contenido, hagamos _lo primero que hay que hacer_ en cualquier proyecto de software...

## Iniciar git

Si no lo habéis hecho todavía, cread una cuenta en [GitLab](https://gitlab.com/).

Una vez tengáis la cuenta activa, cread un proyecto (también llamado repositorio):

- Puede ser privado si queréis
- No le digáis que cree un archivo `README.md`

En mi caso, el proyecto se encuentra alojado en <https://gitlab.com/sadi78/blog-tutorial-de-jekyll>.

GitLab te dará instrucciones para inicializar el proyecto.

Si es la primera vez que utilizáis git, deberéis configurar vuestro nombre e email. No os recomiendo que utilices vuestro email real, aunque podéis hacerlo si queréis:

```
git config --global user.name "NOMBRE Y APELLIDOS"
git config --global user.email "USERNAME@example.com"
```

A continuación, GitLab muestra instrucciones para crear un nuevo repositorio, subir una carpeta existente o subir un repositorio git existente.

Seguid las instrucciones para subir una carpeta existente, teniendo en cuenta que `existing_folder` es la carpeta `mi-super-web` (o el nombre que le hayáis dado).

### Generar claves SSH

Si os da este error:

```
git@gitlab.com: Permission denied (publickey).
fatal: Could not read from remote repository.
```

entonces tenéis que generar una clave de acceso. Las instrucciones están aquí: <https://docs.gitlab.com/ee/ssh/README.html#generating-a-new-ssh-key-pair>

Para crear la clave en local:

```
ssh-keygen -t ed25519 -C "<comment>"
```

Seguid las instrucciones y, una vez finalizado, copiad el contenido del arhivo `CARPETA_PERSONAL_DEL_USUARIO/.ssh/id_ed25519.pub` en el portapapeles.

En GitLab, id a Configuración (del usuario, no del proyeco) y Claves SSH. Pegad la clave en la caja de texto, dadle el nombre del ordenador en el habéis creado la clave, por ejemplo `macbook personal` o `lenovo del trabajo` y aceptad.

Para comprobar que todo se ha configurado correctamente, ejecutad:

```
ssh -T git@gitlab.com
```

Y volved a ejecutar el comando:

```
git push -u origin master
```

## GitHub Desktop

GitHub Desktop permite gestionar repositorios git alojados en cualquier servidor, no únicamente GitHub.

Una vez abierto, añadid un repositorio local (Ctro+O) y seleccionad la carpeta `mi-super-web`.

A parir de ahora, podremos manejar las versiones/actualizaciones de nuestro sitio web desde aquí.
