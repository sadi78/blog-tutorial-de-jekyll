---
layout: post
title:  Probando diferentes metadatos y contenidos
date:   2020-11-01 20:22:35 +0100
categories:
---
En esta entrada vamos a hacer diferentes pruebas para entender qué tipo de archivos genera Jekyll a partir de nuestros archivos.

Podéis ver los archivos de prueba aqui: <https://gitlab.com/sadi78/blog-tutorial-de-jekyll/-/tree/master/pruebas>.

<style>
.screenshot {
    border: 1px solid grey;
}
</style>

## Archivos HTML

Vamos a poner el mismo contenido en varios archivos para ver qué ocurre cuando cambiamos los metadatos.

**`html1.html`**

Sin metadatos.

```html
<h1>Título</h1>

<h2>Subtítulo</h2>

<p>Párrafo con <strong>negrita</strong>.</p>
```

Podemos ver este archivo en la siguiente ruta: [/pruebas/html1.html]({{ '/pruebas/html1.html' | relative_url }}):

![]({{ '/img/pruebas_html1.html.png' | relative_url }}){:.screenshot}

Podemos comprobar que el contenido del archivo se ha copiado tal cual al archivo generado.

**`html2.html`**

Con los metadatos vacíos.

```html
---
---
<h1>Título</h1>

<h2>Subtítulo</h2>

<p>Párrafo con <strong>negrita</strong>.</p>
```

Podemos ver este archivo en la siguiente ruta: [/pruebas/html2.html]({{ '/pruebas/html2.html' | relative_url }}):

![]({{ '/img/pruebas_html2.html.png' | relative_url }}){:.screenshot}

Al igual que en el caso anterior, el contenido del archivo también se ha copiado tal cual al archivo generado.

**`html3.html`**

Ahora vamos a meter algunos metadatos de los que Jekyll conoce.

```html
---
layout: default
---
<h1>Título</h1>

<h2>Subtítulo</h2>

<p>Párrafo con <strong>negrita</strong>.</p>
```

Con la variable `layout: default` le estamos diciendo a Jekyll que muestre este contenido dentro de la plantilla `default`. Hay otras plantillas, como `page`, `post`, e incluso podemos crear las nuestras. Ya lo veremos mas adelante.

Podemos ver este archivo en la siguiente ruta: [/pruebas/html3.html]({{ '/pruebas/html3.html' | relative_url }}):

![]({{ '/img/pruebas_html3.html.png' | relative_url }}){:.screenshot}

Ahora, el contenido del archivo se ha introducido dentro de la plantilla y lo vemos "rodeado" por el encabezado y pie de página de nuestro sitio web.

**`html4.html`**

También le podemos decir a Jekyll que ponga el archivo en otra ruta: Aunque no exista, la creará para nosotros.

No solo eso, sino que le podemos dar al archivo otro nombre y extensión.

```html
---
layout: default
permalink: /una/ruta/inventada/html9.txt
---
<h1>Título</h1>

<h2>Subtítulo</h2>

<p>Párrafo con <strong>negrita</strong>.</p>
```

Con la variable `permalink` le decimos a Jekyll la ruta del archivo generado. Fijaos en que la extensión es `.txt`, por lo que, aunque el contenido será perfecto HTML, será "entregado" al navegador como texto.

A este archivo no se accede por la ruta `/pruebas/html4.html`, sino por la ruta especificada en `permalink`, [/una/ruta/inventada/html9.txt]({{ '/una/ruta/inventada/html9.txt' | relative_url }}):

![]({{ '/img/pruebas_html9.txt.png' | relative_url }}){:.screenshot}

Ahora, el contenido del archivo también se ha introducido dentro de la plantilla y está "rodeado" por el encabezado y pie de página HTML de nuestro sitio web, pero a diferencia de antes, se muestra como texto plano.

## Archivos Markdown

Vamos a poner el mismo contenido en todos los archivos para ver qué ocurre cuando cambiamos los metadatos,
pero vamos a avanzar un poco más metiendo algunas variables en el contenido.

**Nota**: Las llaves deben ir juntas sin espacio, pero las muestro aquí separadas (`{ {` y `} }`) porque de lo contrario Jekyll las interpretaría y no se mostrarían.

**`md1.md`**

Sin metadatos.

```markdown
# Título

## Sección

Párrafo con **negrita**.

- Nombre del sitio:  { { site.title } }
- Plantilla: { { page.layout } }
- Mi variable: { { mi_variable } }
```

Podemos ver este archivo en la siguiente ruta: [/pruebas/md1.md]({{ '/pruebas/md1.md' | relative_url }}):

![]({{ '/img/pruebas_md1.md.png' | relative_url }}){:.screenshot}

Podemos comprobar que el contenido del archivo se ha copiado tal cual al archivo generado.

La extensión sigue siendo `.md` porque al no tener metadatos, Jekyll no hace nada con el archivo. Lo copia y ya.

**`md2.md`**

Con los metadatos vacíos.

```markdown
---
---
# Título

## Sección

Párrafo con **negrita**.

- Nombre del sitio: { { site.title } }
- Plantilla: { { page.layout } }
- Mi variable: { { mi_variable } }
```

Podemos ver este archivo en la siguiente ruta: [/pruebas/md1.md]({{ '/pruebas/md2.html' | relative_url }}):

![]({{ '/img/pruebas_md2.html.png' | relative_url }}){:.screenshot}

¡Aquí ya vemos cambios!

- Jekyll ha convertido el texto Markdown en HTML.
- Ha ejecutado las instrucciones Liquid (el lenguaje de plantillas), que en este caso eran simplemente mostrar el valor de algunas variables.
- Solo ha mostrado el valor de la primara variable, porque es general del sitio web. Como las dos siguientes son específicas de la página actual y no hay metadatos, se muestran vacías.
- Al no encontrar una variable `layout`, el contenido generado se muestra direcamente, sin integrarlo en ninguna plantilla.
- La extensión del archivo generado es `.html`.

**`md3.md`**

Ahora vamos a meter algunos metadatos, tanto "de los que Jekyll conoce" como propios.

```markdown
---
layout: default
mi_variable: 123
---
# Título

## Sección

Párrafo con **negrita**.

- Nombre del sitio: { { site.title } }
- Plantilla: { { page.layout } }
- Mi variable: { { page.mi_variable } }
```

Ya hemos visto antes que la variable `layout` le dice a Jekyll qué plantilla utilizar para mostrar nuestro contenido.

La variable `mi_variable` es invención nuestra, no le dice nada a Jekyll, pero podemos utilizarla como mejor nos convenga.

Podemos ver este archivo en la siguiente ruta: [/pruebas/md1.md]({{ '/pruebas/md3.html' | relative_url }}):

![]({{ '/img/pruebas_md3.html.png' | relative_url }}){:.screenshot}

Ahora, el contenido del archivo se ha introducido dentro de la plantilla y lo vemos "rodeado" por el encabezado y pie de página de nuestro sitio web.

**Importante**: Este es el método "preferido" para crear contenido en Jekyll.

## Otros tipos de archivos

Cualquier archivo que Jekyll encuentre con cualquier otra extensión será copiado tal cual a nuestro sitio web.

- Documentos de texto: `.txt`
- Archivos web: `.js`, `.css`, `.json`
- Documentos de Office: `.docx`, `.xlsx`, `.csv`
- ...
- Incluso `.yml`