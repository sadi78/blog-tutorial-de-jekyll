---
layout: post
title:  Variables disponibles
date:   2020-11-11 22:22:35 +0100
categories:
---
Vamos a ver qué variables tenemos disponibles para mostrar en nuestras plantillas, páginas y entradas del blog.

Para ver qué es cada variable se recomienda ver esta página: <https://jekyllrb.com/docs/variables/>. Aquí solo veremos algunas de las variables disponibles y su contenido.

## site

`site` contiene las variables globales de nuestro sitio web.

| Key | Value |
|-----|-------|
| `tags` | {{ site.tags }} |
| `categories` | {{ site.categories }} |
| `time` | {{ site.time }} |
| `source` | {{ site.source }} |
| `destination` | {{ site.destination }} |
| `layouts_dir` | {{ site.layouts_dir }} |
| `includes_dir` | {{ site.includes_dir }} |
| `encoding` | {{ site.encoding }} |
| `limit_posts` | {{ site.limit_posts }} |
| `show_drafts` | {{ site.show_drafts }} |
| `future` | {{ site.future }} |
| `baseurl` | {{ site.baseurl }} |
| `show_dir_listing` | {{ site.show_dir_listing }} |
| `permalink` | {{ site.permalink }} |
| `paginate_path` | {{ site.paginate_path }} |
| `title` | {{ site.title }} |
| `email` | {{ site.email }} |
| `description` | {{ site.description }} |
| `url` | {{ site.url }} |
| `theme` | {{ site.theme }} |
| `show_excerpts` | {{ site.show_excerpts }} |
|-----|-------|

## page

`page` contiene las variables locales de la página o entrada actual del blog.

| Key | Value |
|-----|-------|
| `tags` | {{ page.tags }} |
| `url` | {{ page.url }} |
| `collection` | {{ page.collection }} |
| `title` | {{ page.title }} |
| `categories` | {{ page.categories }} |
| `path` | {{ page.path }} |
| `date` | {{ page.date }} |
| `id` | {{ page.id }} |
| `draft` | {{ page.draft }} |
| `layout` | {{ page.layout }} |
| `slug` | {{ page.slug }} |
| `ext` | {{ page.ext }} |
|-----|-------|

## site.related_posts

`site.related_posts` contiene una lista de posts relacionados con el actual. Por ello solo estará disponible si la página actual es una entrada del blog.

Por defecto, serán las 10 entradas mas recientes. Si se quiere que de verdad muestre entradas similares a la actual, mediante un análisis textual de su contenido, habrá que instalar una extensión de Jekyll. Pero esto lo veremos mas adelante.

A continuación se muestra el primer post relacionado con esta misma entrada:

{%- for related in site.related_posts limit: 1 %}

| Key | Value |
|-----|-------|
| `date` | {{ related.date }}|
| `id` | {{ related.id }}|
| `path` | {{ related.path }}|
| `title` | {{ related.title }}|
| `url` | {{ related.url }}|
|-----|-------|

{%- endfor %}

## page.previous y page.next

`page.previous` y `page.next` contienen la entrada anterior y siguiente del blog. También aplica solo a entradas del blog.

| Key | Value |
|-----|-------|
| `page.previous.date` | {{ page.previous.date }}|
| `page.previous.id` | {{ page.previous.id }}|
| `page.previous.path` | {{ page.previous.path }}|
| `page.previous.title` | {{ page.previous.title }}|
| `page.previous.url` | {{ page.previous.url }}|

| Key | Value |
|-----|-------|
| `page.next.date` | {{ page.next.date }}|
| `page.next.id` | {{ page.next.id }}|
| `page.next.path` | {{ page.next.path }}|
| `page.next.title` | {{ page.next.title }}|
| `page.next.url` | {{ page.next.url }}|
|-----|-------|
