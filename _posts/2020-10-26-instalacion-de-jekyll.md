---
layout: post
title:  Instalación de Jekyll
date:   2020-10-26 22:22:35 +0100
categories:
---

Vamos a ver cómo instalar Jekyll y el software necesario para trabajar con él.

Las órdenes de línea de comandos que pondré serán válidas con Linux, aunque se puede utilizar MacOs o Windows sin problema.

### Ruby

Jekyll está programado en [Ruby](https://www.ruby-lang.org/en/), por lo que _antes_ de poder instalarlo y utilizarlo es necesario instalar Ruby junto con sus herramientas de desarrollo.

Tranquilos, no hay que saber _absolutamente nada_ de Ruby ~~ni de programación en general~~ para crear un sitio web con Jekyll.

```
sudo apt install ruby ruby-dev
```

Descarga para Windows: <https://rubyinstaller.org/>. No olvidéis descargar la versión con Devkit.

Para comprobar que se ha instalado correctamente, ejecutad esto en la línea de comandos:

```
ruby --version
```

En mi caso, este es el resultado:

```
ruby 2.7.0p0 (2019-12-25 revision 647ee6f091) [x86_64-linux-gnu]
```

### Jekyll

Ahora ya podemos instalar Jekyll:

```
sudo gem install bundler jekyll
```

Para comprobar que se ha instalado correctamente, ejecutad esto:

```
jekyll --version
```

En mi caso, este es el resultado:

```
jekyll 4.1.1
```



### Git

```
sudo apt install git
```

Descarga para Windows: <https://gitforwindows.org/>

Para comprobar que se ha instalado correctamente, ejecutad esto:

```
git --version
```

En mi caso, este es el resultado:

```
git version 2.25.1
```

### Visual Studio Code

Como editor de código yo recomiendo Visual Studio Code, gratuito, aunque podéis utilizar cualquier otro de vuestra preferencia.

Descarga: <https://code.visualstudio.com/>

### GitHub Desktop

Con GitHub Desktop no necesitarás memorizar los comandos de git.

Descarga: <https://desktop.github.com/>

Y con esto, ¡ya tenemos todo listo para crear nuestro blog!
