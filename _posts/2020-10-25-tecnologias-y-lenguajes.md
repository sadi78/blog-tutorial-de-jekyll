---
layout: post
title:  Tecnologías y lenguajes
date:   2020-10-25 18:22:35 +0100
categories:
---
Para _crear y mantener_ un sitio web con Jekyll habrá que familiarizarse y trabajar con las siguientes herramientas/tecnologías/lenguajes:

### [YAML](https://rollout.io/blog/yaml-tutorial-everything-you-need-get-started/), lenguaje de datos

  _Cheatsheet_: <http://lzone.de/cheat-sheet/YAML>

  YAML es una forma de expresar _datos_ (números, texto, listas, pares clave-valor, etc.) muy fácil de escribir y leer por los humanos. Se utiliza para _configurar_ todo en Jekyll, desde el sitio web en general hasta páginas específicas. Gracias a YAML podemos evitar trabajar con XML o JSON. Aunque no hay que convertirse en un experto, hay que aprender lo mas básico.
  
### [Markdown](https://daringfireball.net/projects/markdown/syntax), lenguaje de marcado simplificado
  
  _Cheatsheet_: <https://aksakalli.github.io/jekyll-doc-theme/docs/cheatsheet/>

  Se trata de una forma muy fácil de escribir _contenido_. Nos facilita la vida a la hora de crear documentos con encabezados a diferentes niveles, negrita, cursiva, hiperenlaces, listados con puntos o numerados... incluso tablas. Y todo esto sin escribir una línea de HTML. Es el lenguaje que más utilizaremos en el día a día, una vez el sitio web esté finalizado y solo tengamos que preocuparnos de añadir contenido. Ya veréis que es muy fácil de aprender e incluso agradable de utilizar.
  
### HTML, el lenguaje de marcado estándar de la web
  
  _Cheatsheet_: <https://htmlcheatsheet.com/>

  Aunque utilizaremos Markdown para crear nuestro contenido, al final ese contenido se verá en un navegador web, que solo acepta HTML. Por este motivo, Jekyll utiliza un _sistema de plantillas_ para generar el HTML final a partir de nuestros archivos de contenido.

  Las plantillas son trozos de código HTML + Liquid en el que se insertan donde nosotros digamos diferentes valores generales del sitio web, como el nombre o nuestro email de contacto, o específicos de la página que estemos mostrando, como el título, la fecha o el contenido en sí. También podemos insertar otras "subplantillas". 

  Si no queremos modificar el tema visual que esté instalado, no tendremos que utilizar HTML en ningún momento, ¡aunque siempre es conveniente saber algo si vamos a mantener una página web!

### [Liquid](https://shopify.github.io/liquid/), lenguaje de plantillas
  
  _Cheatsheet_: <https://gist.github.com/JJediny/a466eed62cee30ad45e2>

  Es un lenguaje de programación muy simplificado. Nos permite introducir en nuestras plantillas el contenido de los archivos, variables del sitio o del archivo específico que estemos mostrando, recorrer los archivos de una carpeta para crear un listado, transformar texto, y muchas cosas más.
  
  Sirve para decirle a Jekyll: "pon aquí el título del post en mayúsculas", "muestra el extracto, pero solo si es el primer post de la página", "para cada post de esta categoría, haces esto", y cosas así.
  
### CSS, lenguaje de estilos

  _Cheatsheet_: <https://htmlcheatsheet.com/css/>

  Sirve para definir y crear la _presentación_ de un documento escrito en un lenguaje de marcado, HTML en este caso.

  Nos permitirá cambiar el estilo visual (los "pequeños detalles", no la estructura) de los diferentes elementos que componen las páginas de nuestro sitio web.

  No es necesario utilizarlo, pero hacerlo te permitirá adaptar un poco el sitio web a tus gustos.

### [Git](https://www.diegocmartin.com/tutorial-git/), software de control de versiones

  _Cheatsheet_: <https://about.gitlab.com/images/press/git-cheat-sheet.pdf>

  Con esta herramienta podremos guardar los cambios que hagamos en nuestro sitio web, pudiendo restaurar cualquiera de las versiones anteriores en cuestión de segundos. No más meteduras de pata y no más "web2", "web2_final", "web2_final2", etc.
  
  Además, gracias al programa [GitHub Desktop](https://desktop.github.com/), no necesitaremos utilizar `git` desde la línea de comandos ni memorizar todas sus opciones. No obstante, habrá que familiarizarse con los conceptos más básicos.

### [GitLab](https://gitlab.com/), servicio web de desarrollo de software colaborativo

  Este servicio nos permite, entre otras muchas cosas, almacenar el código fuente de nuestra web "en la nube" y recuperarlo desde cualquier ordenador en cualquier momento.

  Además, cada vez que hagamos un cambio en nuestra página, tanto de contenido como visual, podremos ejecutar _automáticamente_ scripts que comprueben que la página se genera correctamente y, solo en caso afirmativo, la suban a nuestro servidor web.

  También se podría utilizar [GitHub](https://github.com/) o [Bitbucket](https://bitbucket.org/).

Una vez creado el sitio, para crear páginas y entradas del blog solo será necesario trabajar con Markdown, _tal vez_ algo de HTML y un poquito de YAML.

Si parece mucho, estad tranquilos, iremos introduciendo y viendo poco a poco cómo se utiliza y cómo encaja cada una de estas piezas hasta formar un puzle perfecto.
