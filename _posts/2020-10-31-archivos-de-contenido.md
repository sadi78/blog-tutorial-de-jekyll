---
layout: post
title:  Archivos de contenido
date:   2020-10-31 20:22:35 +0100
categories:
---

En Jekyll hay dos tipos de archivos de contenido: páginas y posts (entradas del blog).

**Muy importante**: Todos los archivos de contenido están compuestos por dos partes: los metadatos y el contenido.

## Metadatos

Los metadatos, o _front matter_, son la configuración específica de la página o entrada del blog, y siguen las siguientes reglas:

- Si se encuentran presentes, siempre es antes que el contenido. Primero metadatos, luego contenido.

- Como toda configuración en Jekyll, está escrita en YAML.

- Para decirle a Jekyll que un archivo tiene metadatos, y dónde terminan éstos y empieza el contenido,
  se utilizan tres guiones `---` en la primera línea del archivo y otros tes guiones entre los metadatos y el contenido.

**Consejo**: Puedes ver el código fuente de cualquier página de este sitio web pinchando en el enlace que encontrarás en el pie de página. Para ver los metadatos de esta página, [pincha aquí](https://gitlab.com/sadi78/blog-tutorial-de-jekyll/-/raw/master/{{ page.path }}).

Algunos metadatos son estándar de Jekyll, por ejemplo `layout`, `title` o `date`. Otros nos los podemos inventar nosotros para utilizarlos como queramos. Por ejemplo, podemos añadir a cualquier página o post una variable `principal: true` y luego, en la plantilla de la página principal, mostrar arriba como destacadas las páginas y entradas que tengan esta variable (y sea cierta). O podemos añadir una variable `autor: Fulano Mengano` y, en la plantilla de los posts, mostrar el autor bajo el título.

**Recuerda**: puedes ver la página con [lo más básico de YAML](/yaml.html).

### Ausencia de metadatos

Si un archivo no tiene metadatos, se copiará _tal cual_ al sitio web (siempre que no comience por guion bajo `_`).

Mas adelante veremos qué ocurre con los archivos dependiendo de los metadatos que utilicemos.

## Contenido

El contenido de las páginas y posts puede escribirse en Markdown o directamente en HTML. Markdown es mucho más fácil e intuitivo, mientras que con HTML podremos controlar mejor la apariencia y disposición de los elementos en el documento, pero deberemos conocer el lenguaje HTML.

Cuando utilizamos Markdown, este se transforma en HTML antes de ser insertado en la plantilla correspondiente.

**Recuerda**: puedes ver la página con [lo más básico de Markdown](/markdown.html).
