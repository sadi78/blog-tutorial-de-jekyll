---
layout: post
title:  Introducción a Jekyll
date:   2020-10-25 12:22:35 +0100
categories:
---
Jekyll es un generador de **sitios web estáticos**: a partir del contenido que los autores/editores crean, aplica unas plantillas y genera un sitio web en HTML.

Vamos a ver algunos conceptos de los sitios web estáticos:

- Cuando trabajamos en nuestra web, bien añadiendo contenido o bien modificando algún aspecto visual, **no trabajamos con "una copia local del sitio web"**, sino con un conjunto de archivos bien diferenciados: de _configuración_, de _plantillas_, de _estilos_, de _datos_ y de _contenido_. Jekyll se encargará de combinar estos archivos para crear el sitio web cuando nosotros se lo digamos.

- La gran ventaja es que una vez diseñado el sitio web, cuando esté todo a nuestro gusto, **crear nuevo contenido es lo mas fácil del mundo**.

- Nosotros mismos tendremos que **publicar el sitio web** generado por Jekyll en algún servidor web. Spoiler: se puede automatizar, y es muy fácil.

- En el sitio web **no hay usuarios**. Todo el mundo ve lo mismo. No hay formulario de login ni de registro.

- Aunque se llame estático, **se puede mostrar información dinámica**. Por ejemplo, se podría mostrar la evolución de un huracán, valores bursátiles o resultados de fútbol en tiempo real. Lo que no se puede hacer es mostrar información específica de un usuario o mostrar/ocultar cosas dependiendo del usuario. Porque no hay usuarios, repito.

  En realidad, hay soluciones alternativas para esto, pero quedan fuera del alcance de este tutorial.

- El sitio **no se administra desde la propia página web**, sino desde una carpeta en nuestro ordenador.

- Cualquier administrador se puede descargar el "código fuente" del sitio web, modificarlo, ver si los cambios están bien (en local) y subirlo.

- **No hay roles de administrador**. Todos los administradores tienen acceso a todo: configuración, plantillas y contenido. Leer y escribir. Eliminar.

  También hay soluciones para esto, pero son complejas y normalmente no valen la pena.

- **No hay base de datos**.

- **No hay un "lenguaje web"**, como PHP o Python.

- Es **muy económico**. Por los dos motivos anteriores, puede alojarse en un servidor web básico. Incluso gratis.

- **Los riesgos de seguridad se reducen** drásticamente. Nunca se puede decir _totalmente_, pero casi.

## Cuándo no hay que utilizar Jekyll

- Si se necesita mostrar información específica a cada usuario.

- Si es indispensable poder crear contenido y subir archivos (imágenes, vídeos) desde el móvil o tablet.

- Si se prevee que el sitio web va a ser grande, con miles de entradas y decenas de autores. No suele ser el caso.

- Si se necesita funcionalidad en el servidor. Por ejemplo, enviar emails, procesar formularios o realizar búsquedas. Aunque cada vez existen mas servicios externos que ofrecen estas funcionalidades.
