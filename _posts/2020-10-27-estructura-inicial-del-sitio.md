---
layout: post
title:  Estructura inicial del sitio
date:   2020-10-27 21:22:35 +0100
categories:
---
Esta es la estructura inicial de nuestro sitio web:

```
mi-super-web
├── 404.html
├── Gemfile
├── Gemfile.lock
├── _config.yml
├── _posts
│   └── 2020-11-07-welcome-to-jekyll.markdown
├── about.markdown
└── index.markdown
```

A continuación veremos qué es cada archivo:

- `404.html` es la página que será mostrada cuando un visitante del sitio intente acceder a una URL que no existe.

- `Gemfile` y `Gemfile.lock` son dos archivos "de sistema". Le dicen a Ruby qué paquetes y plugins instalar cuando
  el código fuente se copia a otro ordenador.

- `_config.yml` es el archivo de configuración general del sitio. Os animo a que lo abráis ahora mismo y veais su contenido.

- `_posts` es una carpeta especial utilizada para generar las entradas de nuestro blog.

- `2020-10-20-welcome-to-jekyll.markdown` es la primera -y única, de momento- entrada de nuestro blog. En vuestro caso la fecha será diferente.

- `about.markdown`, al no estar dentro de `_posts`, es una "página suelta".

- `index.markdown` será la página principal del sitio web.


## Conceptos importantes sobre carpetas y archivos

- Por defecto, algunos archivos son ignorados por Jekyll a la hora de generar el sitio.
  Esta lista de archivos excuídos se puede configurar en `_config.xml`.

  Por ejemplo, se puede crear una carpeta `ideas_para_posts` y añadirla a la lista `exclude` para que no termine publicada en la web. O un archivo `IMPORTANTE_LEER_ANTES_DE_PUBLICAR.txt` (que, por supuesto, nadie leerá).

- Los archivos y carpetas que comienzan por `_` son ignorados.
  Únicamente se utilizarán si son referenciados desde algún otro sitio.

  Siguiendo esta lógica, la carpeta y el archivo del ejemplo anterior se podrían renombrar a `_ideas_para_posts` e `_IMPORTANTE_LEER_ANTES_DE_PUBLICAR.txt` y no haría falta añadirlos a la lista de exclusiones.

- Cualquier otro archivo será "copiado" al sitio web final, posiblemente con algunas transformaciones, como veremos mas adelante.

- Los nombres de archivo de las entradas del blog han de seguir el formato `<año>-<mes>-<día>-nombre-descriptivo.extensión`.

  Si la fecha está en el futuro, la entrada será ignorada.

- Las extensiones `.markdown` y `.md` son lo mismo.
