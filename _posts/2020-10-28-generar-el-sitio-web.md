---
layout: post
title:  Generar el sitio web
date:   2020-10-28 18:22:35 +0100
categories:
---

Para generar nuestro sitio web, utilizaremos el siguiente comando:

```
jekyll build
```

Esto creará una subcarpeta `_site` en la que se almacenará nuesto sitio web. Ya podemos copiar el contenido de esta carpeta a cualquier servidor web y tendremos un blog completamente funcional. Aunque casi sin contenido, eso sí.

En realidad, mientras estemos modificando nuestro sitio en local, lo normal es que ejecutemos:

```
jekyll serve --watch
```

De esta forma:

- Se generará el sitio web, igual que si hubiésemos ejecutado `jekyll build`.
- Jekyll iniciará un servidor web local para que podamos ver nuestro sitio web en vivo. La URL es `http://localhost:4000`.
- Cuando modifiquemos cualquier archivo -excepto `_config.yml`- el sitio será regenerado automáticamente.
  Para ver los cambios, solo tendremos que recargar la página (F5).

Fijaos en el contenido de la carpeta `_site`. Estas son las páginas reales que forman nuestro sitio web, que no tienen por qué corresponderse con los archivos con los que trabajamos nosotros. Por ejemplo, mirad cómo las entradas del blog se encuentran dentro de subcarpetas por año, mes y día, mientras que en `_posts` están todos los archivos juntos en la misma carpeta.
