---
layout: post
title:  Configuración del sitio
date:   2020-10-30 02:22:35 +0100
categories:
---

El sitio web actual tiene mucha información que viene por defecto.
Este es el momento de modificar los valores que estimeis oportunos en `_config.yml` y echar un vistazo a la documentación
sobre la [configuración de Jekyll](https://jekyllrb.com/docs/configuration/).

Estos son los valores que yo tengo para este sitio web. Puedes ver el archivo completo aquí: <https://gitlab.com/sadi78/blog-tutorial-de-jekyll/-/blob/master/_config.yml>

```yaml
title: Blog-tutorial de Jekyll
email: jlsalinas@example.com
description: >- # this means to ignore newlines until "baseurl:"
  Tutorial de Jekyll en español
baseurl: "" # the subpath of your site, e.g. /blog
url: "" # the base hostname & protocol for your site, e.g. http://example.com
twitter_username: jotaelesalinas
github_username:  jotaelesalinas

# Build settings
theme: minima
plugins:
  - jekyll-feed

show_excerpts: true
```

Este archivo está escrito em **YAML**. En la introducción ya vimos que YAML se utiliza para configurar _todo_ en Jekyll.

Para nuestras necesidades, basta con saber esto sobre YAML:

- Cada opción de configuración está expresada por un _par clave-valor_.
- La clave es lo que hay antes de los dos puntos, por ejemplo `title`.
- El valor es lo que viene después de los dos puntos, que puede ser:
  - Un valor simple, como un número, una fecha, una cadena de texto que quepa en una línea o los valores `true` y `false`. Ejemplo: `show_excerpts`.
  - Una cadena de texto multilínea. En ese caso habrá que poner `>-` después de la clave y el texto
    en una o varias líneas nuevas sangradas otro nivel. Ejemplo: `description`.
  - Un listado de _cosas_. En tal caso, se pondrá cada una de ellas en una línea nueva, sangradas todas otro nivel, comenzando por `-` y espacio. Ejemplo: `plugins`. (Puede que no sea el mejor ejemplo, porque solo tiene un elemento.)
- El sangrado importa. Cuando hay sub-elementos, los espacios al principio de la línea determinan en qué nivel se encuentra el elemento. Se recomienda utilizar una sangría de 2 espacios.
- El carácter `#` indica que lo que viene detrás es un comentario y debe ser ignorado.

Si vueles a ejecutar ahora 

```
jekyll serve --watch
```

verás que los cambios han surtido efecto.

Ya estamos preparados para empezar a crear contenido.
