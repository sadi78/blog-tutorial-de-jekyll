---
layout: post
title:  Fin (de la primera parte)
date:   2020-11-10 22:22:35 +0100
categories:
---
Lo creáis o no, con lo que hemos visto hasta ahora ya estáis preparados para crear vuestro sitio web y añadir contenido.
Pero esto no termina aquí. Veamos qué nos espera:

![Fin de la primera parte](https://images-na.ssl-images-amazon.com/images/I/51ptDw%2B7qdL._SX425_.jpg)

- Variables disponibles
- CI/CD
    - Comprobar que el sitio se genera bien y que no tiene enlaces erróneos
    - Subir el sitio a GitLab Pages
    - Subir el sitio a un servidor propio
- Variables disponibles
- Posts relacionados (LSI) <https://jekyll.github.io/classifier-reborn/>
- Colecciones <https://jekyllrb.com/docs/collections/>
- Archivos de datos <https://jekyllrb.com/docs/datafiles/>
- Navegación <https://jekyllrb.com/tutorials/navigation/>

<!--
Plugins:
- Búsqueda: <https://github.com/christian-fei/Simple-Jekyll-Search> (Zolan ya la trae)
- jekyll-paginate -- gem "jekyll-paginate" <https://jekyllrb.com/docs/pagination/>
- jekyll-sitemap -- gem "jekyll-sitemap" <https://github.com/jekyll/jekyll-sitemap>
- jekyll/tagging -- gem "jekyll-tagging" <https://github.com/pattex/jekyll-tagging>
-->
