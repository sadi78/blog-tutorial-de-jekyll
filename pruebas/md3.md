---
layout: default
mi_variable: 123
---
# Título

## Sección

Párrafo con **negrita**.

- Nombre del sitio: {{ site.title }}
- Plantilla: {{ page.layout }}
- Mi variable: {{ page.mi_variable }}
