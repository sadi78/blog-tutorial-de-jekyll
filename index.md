---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: home
---
Hola y bienvenidos a este blog-tutorial de [Jekyll](https://jekyllrb.com/),
un generador para sitios web estáticos sencillos.

Este sitio está creado con Jekyll y alojado en GitLab Pages.

**Importante**: El contenido de este tutorial todavía está en fase de borrador.
